package com.wolox.cameltraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CameltrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CameltrainingApplication.class, args);
	}

}
