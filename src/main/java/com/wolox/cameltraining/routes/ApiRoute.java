package com.wolox.cameltraining.routes;

import com.wolox.cameltraining.processors.ApiProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiRoute extends RouteBuilder {
   @Autowired
   ApiProcessor apiProcessor;
    @Override
    public void configure() throws Exception {
        from("file:directoryOne")
                .process(apiProcessor)
                .to("file:directoryTwo");

        restConfiguration()
                .host("localhost")
                .port(8080).bindingMode(RestBindingMode.auto);

        rest("/api/books/")
                .get("/{isbn}")
                .route()
                .toD("jetty:https://openlibrary.org/api/books?bibkeys=ISBN:${header.isbn}&format=json&bridgeEndpoint=true")
                .unmarshal().json(JsonLibrary.Jackson);

    }
}
